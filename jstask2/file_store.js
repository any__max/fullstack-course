class FileStore {

    constructor()
    {
        this.store = {};
    }

    save(fileName) {
        this.store[fileName] = true;
    }

    has(fileName) {
        return this.store[fileName] !== undefined;
    }

    delete(fileName) {
        this.store[fileName] = undefined;
    }
}


module.exports = {
    FileStore,
  };