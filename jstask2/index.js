const {FileStore} = require('./file_store.js');

if (process.argv.length <= 2) {
    console.log("Usage: " + __filename + " path/to/directory");
    process.exit(-1);
}
const path = process.argv[2];

const fs = require("fs");
let file_store = new FileStore()

setInterval(() => { 
    fs.readdir(path, (err, files) => {
        if (err) {
          console.error(err);
        } else {
          files.forEach(file => {
            if (!file_store.has(file)) {
                file_store.save(file);
                console.log("Добавлен новый файл " + file);
            }
          });
          for (var fileName in file_store.store) {
            if (file_store.has(fileName) && !files.includes(fileName)) {
                file_store.delete(fileName);
                console.log(fileName + " удален");
            }
          }
        }
      });     
}, 1000);
